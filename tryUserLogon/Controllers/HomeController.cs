﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using tryUserLogon.Models;
using tryUserLogon.Extensions;

namespace tryUserLogon.Controllers
{
    public class HomeController : Controller
    {
        LogonStatus _LogonStatus = new LogonStatus();
        public ActionResult Index()
        {
            ViewData["IsAuthenticated"] = "未登入";
            if (_LogonStatus.isSigned())
            {
                ViewData["IsAuthenticated"] = "已登入";
            }

            return View();
        }
        public ActionResult Logon()
        {
           if(_LogonStatus.isSigned())
            {
                return Content("PASS");
            }

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logon(FormCollection LogonCollection)
        {

            var UserName = LogonCollection["UserName"].ifNulltoEmpty();
            var Password = LogonCollection["Password"].ifNulltoEmpty();

            // 登入的密碼（以 SHA1 加密）
            //var PasswordSHA = FormsAuthentication.HashPasswordForStoringInConfigFile(Password, "SHA1");

            //這一條是去資料庫抓取輸入的帳號密碼的方法請自行實做
            //var myAcc = account.GetSingleAccount(acc, Password);

            //做一個假的範例用
            var myAcc = new MyAccount(UserName, Password);
            // 登入時清空所有 Session 資料
            Session.RemoveAll();

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
            myAcc.UserName,//你想要存放在 User.Identy.Name 的值，通常是使用者帳號
            DateTime.Now,
            DateTime.Now.AddMinutes(30),
            false,//將管理者登入的 Cookie 設定成 Session Cookie
            myAcc.Rank.ToString(),//userdata看你想存放啥
            FormsAuthentication.FormsCookiePath);

            string encTicket = FormsAuthentication.Encrypt(ticket);

            Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

            return RedirectToAction("TestIsLogon", "Home");
            //return View();
        }
        [Authorize]
        public ActionResult TestIsLogon()
        {
            return Content("PASS");
        }
    }
}