﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tryUserLogon.Extensions
{
    public static class NulltoEmptyExtension
    {
        public static string ifNulltoEmpty(this string str)
        {
            return str ?? "";
        }
    }
}