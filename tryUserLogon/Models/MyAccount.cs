﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace tryUserLogon.Models
{
    public class MyAccount
    {
        [Required]
        [StringLength(6, ErrorMessage = "帳號不能大於6碼!")]
        [Display(Name = "帳號")]
        public string UserName { get; set; }
        [Required]
        [StringLength(6, ErrorMessage = "密碼不能大於6碼!")]
        [Display(Name = "密碼")]
        public string Password { get; set; }
        public int Rank { get; set; }
        public MyAccount(string _UserName,string _Password)
        {
            string simulatio_DB_Password = "abc123"; //假裝從DB撈回來的
            if (_Password == simulatio_DB_Password)
            {
                this.UserName = _UserName;
                this.Password = simulatio_DB_Password;
                this.Rank = 0;
            }
        }

    }
}